CoursesPlus
---
Hello! This is the official repository for CoursesPlus!

CoursesPlus was originally created by Alex Studer, and has since expanded to include several other as part of the team.

Also on the CoursesPlus Team:
* Joshua Fielding
* William Barkoff
* Sehan Choi
* Eeshan Tripathii